#!/bin/bash
set -euo pipefail
dbg() { echo "+ $*" >&2; "$@"; }
dbg rm -f ./*.pkg.*
dbg makepkg -s --noconfirm --needed .
tmp=$(dbg namcap PKGBUILD ./*.pkg.*)
if [[ -n "$tmp" ]]; then
	printf "%s\n" "$tmp"
	exit 1
fi
echo "ALL FINE!"
