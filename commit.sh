#!/bin/bash
set -euo pipefail
fatal() { echo "ERROR: $*" >&2; exit 1; }

# shellcheck disable=1091,2154
pkgname=$(. PKGBUILD && echo "$pkgname")
dir=$(basename "$PWD")
remote=$(git config --get remote.origin.url)
remote_should_be="ssh://aur@aur.archlinux.org/$dir.git"

if [[ "$remote" != "$remote_should_be" ]]; then
	echo "git init"
	echo "git remote add origin $remote_should_be"
	echo "git remote set-url origin $remote_should_be"
	fatal "Remote $remote is not $remote_should_be"
fi

if [[ "$dir" != "$pkgname" ]]; then
	fatal "Directory has wrong name - $dir != $pkgname"
fi

###############################################################################

if [ ! -e .gitignore ]; then
	cat <<EOF > .gitignore
pkg
src
*.pkg.*
EOF
fi
set -x
makepkg --printsrcinfo > .SRCINFO
git add PKGBUILD .SRCINFO .gitignore
git commit -m "$*"


