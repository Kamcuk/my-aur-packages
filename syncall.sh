#!/bin/bash
set -xeuo pipefail
tmp=$(
	curl 'https://aur.archlinux.org/rpc/?v=5&type=search&by=maintainer&arg=Kamilcuk' |
	jq -r '.results[].PackageBase'
)
for name in $tmp; do
	(
	if cd "$name"; then
		git pull
	else
		git clone "ssh://aur@aur.archlinux.org/$name.git" "$name"
	fi
	)
done


